@extends('layouts.app')

@section('content')
<h1 style="padding-left:50px">Create a new task!</h1>
<form method="post" action="{{action('TaskController@store')}}" style="padding-left:50px">
@csrf
          <div class="form-group">    
              <label for="title">Task Title : </label>
              <input type="text" class="form-control" name="title"/>
          </div>
          <div class="form-group">    
              <input type="submit" class="btn btn-primary" name="submit" value = "Save New Task"/>
          </div>          
</form> 
@endsection