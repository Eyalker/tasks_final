@extends('layouts.app')

    @section('content')
    <H1>All tasks</H1>
    @isset($filtered)
     <a href = {{route('tasks.index')}}>All Tasks</a>
    @else
     <a href = {{route('myfilter')}}>My tasks</a>
    @endisset
    <ul>
        @foreach($tasks as $task)
        <li>
            {{$task->title}} <a href="{{route('tasks.edit', $task->id)}}">Edit</a> 
            @can('admin')
            <a href="{{route('delete', $task->id)}}">Delete</a>
            @endcan
           
            @if ($task->status == 0)
                @can('admin')
                <a href="{{route('done', $task->id)}}">Mark As done</a>
                @endcan 
            @else
            Done!
            @endif
        </li>
        @endforeach
    </ul>  
    <a href="{{route('tasks.create')}}">Create a new task</a>
    
    @endsection

    

